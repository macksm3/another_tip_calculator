# This is a sample Python script.

# Start with imports.


# Define object classes and functions here.
class TipCalc:

    def __init__(self, bill_amount):
        self.myOutput = None
        self.bill_amount = float(bill_amount)
        self.option = "0"
        self.inputValue = 0.0
        self.tip_percent = 0.0
        self.tip_amount = 0.0
        self.total = 0.0
        # print(f'meal cost ${"{:,.2f}".format(self.bill_amount)}, how do you want to calculate the tip?')
        # self.how_to_calculate()

    def output_results(self):
        msg = f'\nmeal bill is ${"{:,.2f}".format(self.bill_amount)} ' \
              f'\ntip percent is {self.tip_percent}% ' \
              f'\ntip amount is ${"{:,.2f}".format(self.tip_amount)} ' \
              f'\ntotal to pay is ${"{:,.2f}".format(self.total)} \n'
        return msg

    def tip_by_percent(self, tip_percent):
        print("fired tip by percent method")
        self.tip_percent = float(tip_percent)
        self.tip_amount = round(self.bill_amount * (self.tip_percent / 100), 2)
        self.total = self.bill_amount + self.tip_amount
        # return f'Entered tip percent: ${"{:,.2f}".format(self.tip_percent)}'

    def tip_by_amount(self, tip_amount):
        print("fired tip by amount method")
        # self.tip_amount = float(input("enter amount of tip: "))
        self.tip_amount = float(tip_amount)
        self.tip_percent = round((self.tip_amount / self.bill_amount) * 100, 2)
        self.total = self.bill_amount + self.tip_amount
        # return f'Entered tip amount: ${"{:,.2f}".format(self.tip_amount)}'

    def tip_from_total(self, total):
        print("fired tip from total method")
        # self.total = "{:,.2f}".format(float(input("enter total paid: ")))
        # self.total = float(input("enter total paid: "))
        self.total = float(total)
        self.tip_amount = (self.total - self.bill_amount)
        self.tip_percent = round((self.tip_amount / self.bill_amount) * 100, 2)
        #  f'Entered total paid: ${"{:,.2f}".format(self.total)}'
        # print(f'Entered total paid: ${"{:,.2f}".format(self.total)}')

    '''
    options = {
        0: tip_by_percent,
        1: tip_by_amount,
        2: tip_from_total
    }
    options = {"0": tip_by_percent(value), "1": tip_by_amount(value), "2": tip_from_total(value)}
    '''

    def calculate(self, option, value):
        # choice = input("how do you want to calculate the tip?")
        self.option = str(option)
        self.inputValue = float(value)
        if option == 0:
            self.tip_by_percent(value)
        elif option == 1:
            self.tip_by_amount(value)
        elif option == 2:
            self.tip_from_total(value)
        else:
            print("option not recognized")
        print("fired calculate method")
        # options = {"0": self.tip_by_percent(value), "1": self.tip_by_amount(value), "2": self.tip_from_total(value)}
        # myOutput = self.options.get(self.option(self.inputValue))
        # self.myOutput = options.get(self.option)
        # print(self.myOutput)
        # if choice == 3: tip_from_total(self.inputValue)


def main():
    # this is where the main body of code goes.
    # a single input from GUI paired with result of choice via radio buttons

    # enter amount of bill for meal and create object
    bill = float(input("what is the total bill? "))
    myTip = TipCalc(bill)

    # choice 1 is entering percentage of tip
    tipPercent = float(input("enter percent of tip"))
    myTip.tip_by_percent(tipPercent)
    print(myTip.output_results())
    # choice 2 is entering amount of tip
    tipAmount = float(input("enter amount of tip: "))
    myTip.tip_by_amount(tipAmount)
    print(myTip.output_results())
    # choice 3 is tip by total paid
    grandTotal = float(input("enter total paid: "))
    myTip.tip_from_total(grandTotal)
    # myTip.tip_from_total()
    print(myTip.output_results())


if __name__ == '__main__':
    main()

# end of program
