## Another Tip Calculator

Have you ever received your restaurant bill and 
wanted to leave the fewest paper currency notes possible, 
but wonder if that includes an appropriate tip amount?

#### With "My Tip Calculator" it is easy to find out!

![image](tip_calculator_start.png)

Start by entering the amount of the bill

![image](tip_calculator_options.png)

The bill amount gets locked in and now the desired option can be selected 
and the value entered. 

![image](tip_calculator_output.png)

Try different options and amounts and get new results when you click submit button

The file menu has a start over option to enter a new bill amount

see the repo at https://gitlab.com/macksm3/another_tip_calculator

expansion of project done during class

### Design objectives

Use dictionary instead of if else for choice selection

Create modules for class creation and import to instantiate

Exchange data between modules


