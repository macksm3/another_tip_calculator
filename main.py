# tip calculator main file will import modules,
# instantiate objects and call methods.

import BuildWindow as bw
import TipCalc as tc

# from BuildWindow import *

# make global start_value
start_value = 0.001


def start_calculator():
    myTip = tc.TipCalc(start_value)
    myTip.calculate()


# def getBillAmount():


def build_window():
    myApp = bw.BuildWindow("My Tip Calculator")
    if myApp.billTotalEntry is float:
        print("found something")
    global start_value
    start_value = myApp.billTotalEntry
    print(f"build window call, start value = {start_value}")
    if myApp.billEntered:
        myApp.process_entry
    else:
        print("process entry return false")
    myApp.mainloop()


if start_value != 0.001:
    start_calculator()


def main():
    build_window()


if __name__ == '__main__':
    main()

# end of program
