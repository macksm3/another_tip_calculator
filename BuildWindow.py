# This is a sample Python script.

# Start with imports.
import tkinter as tk
from tkinter import font
import TipCalc as TC


# Define object classes and functions here.
class BuildWindow(tk.Tk):
    # object attributes here,
    billTotalEntry = 0.002

    def __init__(self, appName):
        super().__init__()
        self.title(str(appName))
        self._create_menu()
        self._create_bill_entry_frame()
        self.billEntered = False
        self.calcType = 0
        self.optionValue = float
        # print(self.title)

    def _create_menu(self):
        menu_bar = tk.Menu(master=self)
        self.config(menu=menu_bar)
        file_menu = tk.Menu(master=menu_bar)
        file_menu.add_command(label="Start Over", command=self.reset_calculator)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=quit)
        menu_bar.add_cascade(label="File", menu=file_menu)

    def process_entry(self):
        # while not self.billEntered:
        #     print("still waiting")
        #     # wait for it
        # print("button pressed")
        self.billTotalEntry = self.bill_entry.get()
        # print(self.billTotalEntry)
        self.bill_entry.config(state="disabled")
        self.first_submit.config(state="disabled")
        self.create_option_entry_frame()
        return self.billTotalEntry

    def _create_bill_entry_frame(self):
        def submitOne():
            self.process_entry()

        # create a frame
        bill_entry_frame = tk.Frame(master=self)
        bill_entry_frame.pack(fill=tk.X)

        # a label for the app title
        self.display = tk.Label(
            master=bill_entry_frame,
            text="Tip Calculator",
            font=font.Font(size=20, weight="bold"),
        )
        self.display.pack()

        first_entry_frame = tk.Frame(master=self)
        first_entry_frame.pack()

        # a label with instruction
        self.direction = tk.Label(
            master=first_entry_frame,
            text="Enter amount of bill $",
            font=font.Font(size=15, weight="bold"),
        )
        self.direction.pack(side="left")

        # entry field for bill_amount
        self.bill_entry = tk.Entry(
            master=first_entry_frame,
            width=7,
        )
        self.bill_entry.pack(side="left")

        # button to submit
        self.first_submit = tk.Button(
            master=self,
            text="Next",
            command=submitOne,
        )
        self.first_submit.pack()

    def get_calc_type(self):
        print(f'chosen calculation type {self.calcType}')

    def create_option_entry_frame(self):
        options = ("Tip By percent", "Tip by amount", "Tip from total")
        instructions = ("Enter tip percent %", "Enter tip amount $", "Enter total to pay $")
        '''
        options = {
            0: "Tip By percent",
            1: "Tip by amount",
            2: "Tip from total"
        }
        instructions = {
            0: "Enter tip percent",
            1: "Enter tip amount",
            2: "Enter total to pay"
        }
        '''

        def set_option():
            self.calcType = f.get()
            print(options[f.get()])  # when options is tuple
            # print(options.get(f.get())) # when options id dict
            self.get_calc_type()
            valueInstruction.config(text=instructions[self.calcType])

        def submitOption():
            self.optionValue = valueEntry.get()
            myTip = TC.TipCalc(self.billTotalEntry)
            myTip.calculate(self.calcType, self.optionValue)
            # return the msg from output_results method
            outputMessage.config(text=myTip.output_results())
            print(f"submit {self.calcType} & {self.optionValue}")

        # create a frame
        self.option_entry_frame = tk.Frame(master=self)
        self.option_entry_frame.pack(fill=tk.X)
        # show and return choice and value
        # a label with instruction
        optionInstruction = tk.Label(
            master=self.option_entry_frame,
            text="How do you want to calculate the tip?",
            # font=font.Font(size=15, weight="bold"),
        )
        optionInstruction.pack()
        # return # chioce from radio buttons
        f = tk.IntVar()
        radioFrame = tk.Frame(master=self.option_entry_frame)
        radioFrame.pack()
        for index in range(len(options)):
            radiobutton = tk.Radiobutton(radioFrame,
                                         text=options[index],
                                         variable=f,
                                         value=index,
                                         command=set_option
                                         )
            radiobutton.pack()

        valueFrame = tk.Frame(master=self.option_entry_frame)
        valueFrame.pack()
        valueEntry = tk.Entry(master=valueFrame, width=8)
        valueEntry.pack(side="right")
        valueInstruction = tk.Label(
            master=valueFrame,
            text="Enter something",
            font=font.Font(size=15, weight="bold"),
        )
        valueInstruction.pack(side="right")

        # button to submit
        option_submit = tk.Button(
            master=self.option_entry_frame,
            text="Submit",
            command=submitOption,
        )
        option_submit.pack()
        # need output frame and output label
        self.outputFrame = tk.Frame(master=self)
        self.outputFrame.pack()
        outputMessage = tk.Label(
            master=self.outputFrame,
            text="output message here",
            font=font.Font(size=15, weight="bold"),
            justify="right",
        )
        outputMessage.pack()

    '''
    def _update_display(self, msg, color="black"):
        self.display["text"] = msg
        self.display["fg"] = color
    '''

    def reset_calculator(self):
        """Reset the calculator to run again."""

        self.bill_entry.config(state="normal")
        self.bill_entry.delete(0, "end")
        self.first_submit.config(state="normal")
        self.option_entry_frame.destroy()
        self.outputFrame.destroy()
        # clear both inputs
        # turnoff or destroy option_entry_frame and output_frame
        # self._update_display(msg="This is the reset message, Ready?")


'''
    def get_bill_amount(self):
        print(self.bill_entry.get())
        return self.bill_entry.get()
'''


def build_window():
    myApp = BuildWindow("My Tip Calculator")
    # myApp.add_options_display()
    # bill_amount_variable = myApp.get_bill_amount()
    # print(bill_amount_variable)
    myApp.mainloop()


def main():
    # this is where the main body of code goes.
    build_window()


if __name__ == '__main__':
    main()

# end of program
